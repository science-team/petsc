From 8143cb67ff616ea6511f6541d313887f6270faf7 Mon Sep 17 00:00:00 2001
From: Satish Balay <balay@mcs.anl.gov>
Date: Wed, 6 Nov 2024 18:53:27 -0600
Subject: [PATCH 1/2] configure: add -with-library-name-suffix=<suffix> option

---
 config/PETSc/Configure.py                     | 33 ++++++++++++-------
 config/PETSc/options/sharedLibraries.py       |  3 ++
 .../examples/arch-ci-linux-gcc-ifc-cmplx.py   |  1 +
 doc/changes/dev.rst                           |  1 +
 gmakefile                                     | 21 ++++++------
 makefile                                      | 10 +++---
 6 files changed, 42 insertions(+), 27 deletions(-)

Index: petsc/config/PETSc/Configure.py
===================================================================
--- petsc.orig/config/PETSc/Configure.py	2024-12-08 19:16:25.799514284 +0100
+++ petsc/config/PETSc/Configure.py	2024-12-08 19:16:25.795514257 +0100
@@ -43,6 +43,7 @@
     help.addArgument('PETSc', '-with-default-arch=<bool>',                   nargs.ArgBool(None, 1, 'Allow using the last configured arch without setting PETSC_ARCH'))
     help.addArgument('PETSc','-with-single-library=<bool>',                  nargs.ArgBool(None, 1,'Put all PETSc code into the single -lpetsc library'))
     help.addArgument('PETSc','-with-fortran-bindings=<bool>',                nargs.ArgBool(None, 1,'Build PETSc fortran bindings in the library and corresponding module files'))
+    help.addArgument('PETSc', '-with-library-name-suffix=<string>',          nargs.Arg(None, '', 'Add a suffix to PETSc library names'))
     help.addArgument('PETSc', '-with-ios=<bool>',                            nargs.ArgBool(None, 0, 'Build an iPhone/iPad version of PETSc library'))
     help.addArgument('PETSc', '-with-display=<x11display>',                  nargs.Arg(None, '', 'Specifiy DISPLAY environmental variable for use with MATLAB test)'))
     help.addArgument('PETSc', '-with-package-scripts=<pyscripts>',           nargs.ArgFileList(None,None,'Specify configure package scripts for user provided packages'))
@@ -453,12 +454,16 @@
     # Use build dir here for 'make check' to work before 'make install'
     PREINSTALL_LIB_DIR = os.path.join(self.petscdir.dir,self.arch.arch,'lib')
 
+    self.LIB_NAME_SUFFIX = self.framework.argDB['with-library-name-suffix']
+    self.addMakeMacro('LIB_NAME_SUFFIX', self.LIB_NAME_SUFFIX)
+    self.addDefine('LIB_NAME_SUFFIX', '"'+self.LIB_NAME_SUFFIX+'"')
+
     if self.framework.argDB['with-single-library']:
-      self.petsclib = '-lpetsc'
+      self.petsclib = '-lpetsc'+self.LIB_NAME_SUFFIX
       self.addDefine('USE_SINGLE_LIBRARY', '1')
-      self.addMakeMacro('LIBNAME','${INSTALL_LIB_DIR}/libpetsc.${AR_LIB_SUFFIX}')
+      self.addMakeMacro('LIBNAME','${INSTALL_LIB_DIR}/libpetsc${LIB_NAME_SUFFIX}.${AR_LIB_SUFFIX}')
       self.addMakeMacro('SHLIBS','libpetsc')
-      self.addMakeMacro('PETSC_WITH_EXTERNAL_LIB',self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR, '-lpetsc']+self.packagelibs+self.complibs))
+      self.addMakeMacro('PETSC_WITH_EXTERNAL_LIB',self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR, '-lpetsc'+self.LIB_NAME_SUFFIX]+self.packagelibs+self.complibs))
       self.addMakeMacro('PETSC_SYS_LIB','${PETSC_WITH_EXTERNAL_LIB}')
       self.addMakeMacro('PETSC_VEC_LIB','${PETSC_WITH_EXTERNAL_LIB}')
       self.addMakeMacro('PETSC_MAT_LIB','${PETSC_WITH_EXTERNAL_LIB}')
@@ -468,15 +473,20 @@
       self.addMakeMacro('PETSC_TS_LIB','${PETSC_WITH_EXTERNAL_LIB}')
       self.addMakeMacro('PETSC_TAO_LIB','${PETSC_WITH_EXTERNAL_LIB}')
     else:
-      self.petsclib = '-lpetsctao -lpetscts -lpetscsnes -lpetscksp -lpetscdm -lpetscmat -lpetscvec -lpetscsys'
-      self.addMakeMacro('PETSC_SYS_LIB', self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_VEC_LIB', self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_MAT_LIB', self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_DM_LIB',  self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscdm','-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_KSP_LIB', self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscksp','-lpetscdm','-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_SNES_LIB',self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscsnes','-lpetscksp','-lpetscdm','-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_TS_LIB',  self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetscts','-lpetscsnes','-lpetscksp','-lpetscdm','-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
-      self.addMakeMacro('PETSC_TAO_LIB', self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR,'-lpetsctao','-lpetscts','-lpetscsnes','-lpetscksp','-lpetscdm','-lpetscmat','-lpetscvec','-lpetscsys']+self.packagelibs+self.complibs))
+      pkgs = ['tao', 'ts', 'snes', 'ksp', 'dm', 'mat', 'vec', 'sys']
+      def liblist_basic(libs):
+        return [ '-lpetsc'+lib+self.LIB_NAME_SUFFIX for lib in libs]
+      def liblist(libs):
+        return self.libraries.toStringNoDupes(['-L'+PREINSTALL_LIB_DIR]+liblist_basic(libs)+self.packagelibs+self.complibs)
+      self.petsclib = ' '.join(liblist_basic(pkgs))
+      self.addMakeMacro('PETSC_SYS_LIB', liblist(pkgs[-1:]))
+      self.addMakeMacro('PETSC_VEC_LIB', liblist(pkgs[-2:]))
+      self.addMakeMacro('PETSC_MAT_LIB', liblist(pkgs[-3:]))
+      self.addMakeMacro('PETSC_DM_LIB',  liblist(pkgs[-4:]))
+      self.addMakeMacro('PETSC_KSP_LIB', liblist(pkgs[-5:]))
+      self.addMakeMacro('PETSC_SNES_LIB',liblist(pkgs[-6:]))
+      self.addMakeMacro('PETSC_TS_LIB',  liblist(pkgs[-7:]))
+      self.addMakeMacro('PETSC_TAO_LIB', liblist(pkgs[-8:]))
     self.addMakeMacro('PETSC_LIB','${PETSC_TAO_LIB}')
     self.addMakeMacro('PETSC_LIB_BASIC',self.petsclib)
 
Index: petsc/config/PETSc/options/sharedLibraries.py
===================================================================
--- petsc.orig/config/PETSc/options/sharedLibraries.py	2024-12-08 19:16:25.799514284 +0100
+++ petsc/config/PETSc/options/sharedLibraries.py	2024-12-08 19:16:25.795514257 +0100
@@ -66,9 +66,11 @@
       if self.setCompilers.isDarwin(self.log):
         self.addMakeRule('shared_arch','shared_darwin')
         self.addMakeMacro('SONAME_FUNCTION', '$(1).$(2).dylib')
+        self.addMakeMacro('SONAME_SFX_FUNCTION', '$(1)$(LIB_NAME_SUFFIX).$(2).dylib')
         self.addMakeMacro('SL_LINKER_FUNCTION', '-dynamiclib -install_name $(call SONAME_FUNCTION,$(1),$(2)) -compatibility_version $(2) -current_version $(3) -undefined dynamic_lookup')
       elif self.setCompilers.CC.find('win32fe') >=0:
         self.addMakeMacro('SONAME_FUNCTION', '$(1).dll')
+        self.addMakeMacro('SONAME_SFX_FUNCTION', '$(1)$(LIB_NAME_SUFFIX).dll')
         self.addMakeMacro('SL_LINKER_FUNCTION', '-LD')
         self.addMakeMacro('PETSC_DLL_EXPORTS', '1')
       else:
@@ -76,6 +78,7 @@
         # TODO: check whether we need to specify dependent libraries on the link line (long test)
         self.addMakeRule('shared_arch','shared_linux')
         self.addMakeMacro('SONAME_FUNCTION', '$(1).$(SL_LINKER_SUFFIX).$(2)')
+        self.addMakeMacro('SONAME_SFX_FUNCTION', '$(1)$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX).$(2)')
         self.addMakeMacro('SL_LINKER_FUNCTION', self.framework.getSharedLinkerFlags() + ' -Wl,-soname,$(call SONAME_FUNCTION,$(notdir $(1)),$(2))')
         if config.setCompilers.Configure.isMINGW(self.framework.getCompiler(),self.log):
           self.addMakeMacro('PETSC_DLL_EXPORTS', '1')
Index: petsc/config/examples/arch-ci-linux-gcc-ifc-cmplx.py
===================================================================
--- petsc.orig/config/examples/arch-ci-linux-gcc-ifc-cmplx.py	2024-12-08 19:16:25.799514284 +0100
+++ petsc/config/examples/arch-ci-linux-gcc-ifc-cmplx.py	2024-12-08 19:16:25.795514257 +0100
@@ -25,6 +25,7 @@
   'CXXOPTFLAGS=-g -O',
 
   '--with-scalar-type=complex',
+  '--with-library-name-suffix=CMPLX',
   '--download-hdf5',
   '--with-zlib=1',
   '--download-kokkos=1',
Index: petsc/gmakefile
===================================================================
--- petsc.orig/gmakefile	2024-12-08 19:16:25.799514284 +0100
+++ petsc/gmakefile	2024-12-08 19:16:25.795514257 +0100
@@ -13,6 +13,7 @@
 
 # $(call SONAME_FUNCTION,libfoo,abiversion)
 SONAME_FUNCTION ?= $(1).$(SL_LINKER_SUFFIX).$(2)
+SONAME_SFX_FUNCTION_?= $(1)$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX).$(2)
 # $(call SL_LINKER_FUNCTION,libfoo,abiversion,libversion)
 SL_LINKER_FUNCTION ?= -shared -Wl,-soname,$(call SONAME_FUNCTION,$(notdir $(1)),$(2))
 
@@ -23,19 +24,19 @@
 
 libpetsc_abi_version := $(PETSC_VERSION_MAJOR).$(if $(filter $(PETSC_VERSION_RELEASE), 0 -2 -3 -4 -5),0)$(PETSC_VERSION_MINOR)
 libpetsc_lib_version := $(libpetsc_abi_version).$(PETSC_VERSION_SUBMINOR)
-soname_function = $(call SONAME_FUNCTION,$(1),$(libpetsc_abi_version))
-libname_function = $(call SONAME_FUNCTION,$(1),$(libpetsc_lib_version))
+soname_function = $(call SONAME_SFX_FUNCTION,$(1),$(libpetsc_abi_version))
+libname_function = $(call SONAME_SFX_FUNCTION,$(1),$(libpetsc_lib_version))
 absbasename_all = $(basename $(basename $(basename $(basename $(abspath $(1))))))# arch/lib/libpetsc.so.3.8.0 -> /path/to/arch/lib/libpetsc
 sl_linker_args = $(call SL_LINKER_FUNCTION,$(call absbasename_all,$@),$(libpetsc_abi_version),$(libpetsc_lib_version))
 
-libpetsc_shared  := $(LIBDIR)/libpetsc.$(SL_LINKER_SUFFIX)
+libpetsc_shared  := $(LIBDIR)/libpetsc$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX)
 libpetsc_soname  := $(call soname_function,$(LIBDIR)/libpetsc)
 libpetsc_libname := $(call libname_function,$(LIBDIR)/libpetsc)
-libpetsc_static  := $(LIBDIR)/libpetsc.$(AR_LIB_SUFFIX)
-libpetscpkgs_shared  := $(foreach pkg, $(pkgs), $(LIBDIR)/libpetsc$(pkg).$(SL_LINKER_SUFFIX))
+libpetsc_static  := $(LIBDIR)/libpetsc$(LIB_NAME_SUFFIX).$(AR_LIB_SUFFIX)
+libpetscpkgs_shared  := $(foreach pkg, $(pkgs), $(LIBDIR)/libpetsc$(pkg)$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX))
 libpetscpkgs_soname  := $(foreach pkg, $(pkgs), $(call soname_function,$(LIBDIR)/libpetsc$(pkg)))
 libpetscpkgs_libname := $(foreach pkg, $(pkgs), $(call libname_function,$(LIBDIR)/libpetsc$(pkg)))
-libpetscpkgs_static  := $(foreach pkg, $(pkgs_reverse), $(LIBDIR)/libpetsc$(pkg).$(AR_LIB_SUFFIX))
+libpetscpkgs_static  := $(foreach pkg, $(pkgs_reverse), $(LIBDIR)/libpetsc$(pkg)$(LIB_NAME_SUFFIX).$(AR_LIB_SUFFIX))
 
 ifeq ($(PETSC_WITH_EXTERNAL_LIB),)
   libpetscall_shared  := $(libpetscpkgs_shared)
@@ -148,13 +149,13 @@
   $(call quiet,RANLIB) $@
 endef
 
-%.$(AR_LIB_SUFFIX) : $$(obj) | $$(@D)/.DIR
+%$(LIB_NAME_SUFFIX).$(AR_LIB_SUFFIX) : $$(obj) | $$(@D)/.DIR
 	$(if $(findstring win32fe_lib,$(AR)),$(ARCHIVE_RECIPE_WIN32FE_LIB),$(if $(findstring yes,$(AR_ARGFILE)),$(if $(GMAKE4),$(ARCHIVE_RECIPE_ARGFILE),$(ARCHIVE_RECIPE_DEFAULT)),$(ARCHIVE_RECIPE_DEFAULT)))
 
 # with-single-library=0
-libpkg = $(foreach pkg, $1, $(LIBDIR)/libpetsc$(pkg).$(SL_LINKER_SUFFIX))
+libpkg = $(foreach pkg, $1, $(LIBDIR)/libpetsc$(pkg)$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX))
 define pkg_template
-  $(LIBDIR)/libpetsc$(1).$(AR_LIB_SUFFIX)  : $(call concatlang,$(1))
+  $(LIBDIR)/libpetsc$(1)$(LIB_NAME_SUFFIX).$(AR_LIB_SUFFIX)  : $(call concatlang,$(1))
   $(call libname_function,$(LIBDIR)/libpetsc$(1)) : $(call concatlang,$(1))
 endef
 $(foreach pkg,$(pkgs),$(eval $(call pkg_template,$(pkg))))
@@ -183,7 +184,7 @@
 	$(call quiet,DSYMUTIL) $@
 endif
 
-%.$(SL_LINKER_SUFFIX) : $(call libname_function,%) | $(call soname_function,%)
+%$(LIB_NAME_SUFFIX).$(SL_LINKER_SUFFIX) : $(call libname_function,%) | $(call soname_function,%)
 	@ln -sf $(notdir $<) $@
 
 $(call soname_function,%) : $(call libname_function,%)
Index: petsc/makefile
===================================================================
--- petsc.orig/makefile	2024-12-08 19:16:25.799514284 +0100
+++ petsc/makefile	2024-12-08 19:16:25.795514257 +0100
@@ -92,12 +92,12 @@
 deleteshared:
 	@for LIBNAME in ${SHLIBS}; \
 	do \
-	   if [ -d ${INSTALL_LIB_DIR}/$${LIBNAME}.dylib.dSYM ]; then \
-             echo ${RM} -rf ${INSTALL_LIB_DIR}/$${LIBNAME}.dylib.dSYM; \
-	     ${RM} -rf ${INSTALL_LIB_DIR}/$${LIBNAME}.dylib.dSYM; \
+	if [ -d ${INSTALL_LIB_DIR}/$${LIBNAME}$${LIB_NAME_SUFFIX}.dylib.dSYM ]; then \
+             echo ${RM} -rf ${INSTALL_LIB_DIR}/$${LIBNAME}$${LIB_NAME_SUFFIX}.dylib.dSYM; \
+            ${RM} -rf ${INSTALL_LIB_DIR}/$${LIBNAME}$${LIB_NAME_SUFFIX}.dylib.dSYM; \
 	   fi; \
-           echo ${RM} ${INSTALL_LIB_DIR}/$${LIBNAME}.${SL_LINKER_SUFFIX}*; \
-           ${RM} ${INSTALL_LIB_DIR}/$${LIBNAME}.${SL_LINKER_SUFFIX}*; \
+           echo ${RM} ${INSTALL_LIB_DIR}/$${LIBNAME}$${LIB_NAME_SUFFIX}.${SL_LINKER_SUFFIX}; \
+           ${RM} ${INSTALL_LIB_DIR}/$${LIBNAME}$${LIB_NAME_SUFFIX}.${SL_LINKER_SUFFIX}; \
 	done
 	@if [ -f ${INSTALL_LIB_DIR}/so_locations ]; then \
           echo ${RM} ${INSTALL_LIB_DIR}/so_locations; \
Index: petsc/src/ksp/ksp/tutorials/ex1.c
===================================================================
--- petsc.orig/src/ksp/ksp/tutorials/ex1.c	2024-12-08 19:16:25.799514284 +0100
+++ petsc/src/ksp/ksp/tutorials/ex1.c	2024-12-08 19:16:25.795514257 +0100
@@ -182,6 +182,11 @@
       args: -ksp_monitor_short -ksp_gmres_cgs_refinement_type refine_always
 
    test:
+      suffix: library_preload
+      requires: defined(PETSC_HAVE_DYNAMIC_LIBRARIES) defined(PETSC_USE_SHARED_LIBRARIES)
+      args: -ksp_monitor_short -ksp_gmres_cgs_refinement_type refine_always -library_preload
+
+   test:
       suffix: 2
       args: -pc_type sor -pc_sor_symmetric -ksp_monitor_short -ksp_gmres_cgs_refinement_type refine_always
 
Index: petsc/src/ksp/ksp/tutorials/output/ex1_library_preload.out
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ petsc/src/ksp/ksp/tutorials/output/ex1_library_preload.out	2024-12-08 19:16:25.795514257 +0100
@@ -0,0 +1,13 @@
+  0 KSP Residual norm 0.707107
+  1 KSP Residual norm 0.316228
+  2 KSP Residual norm 0.188982
+  3 KSP Residual norm 0.129099
+  4 KSP Residual norm 0.0953463
+  5 KSP Residual norm < 1.e-11
+Norm of error 2.90785e-15, Iterations 5
+  0 KSP Residual norm 0.353553
+  1 KSP Residual norm 0.0857493
+  2 KSP Residual norm 0.0227273
+  3 KSP Residual norm 0.0060831
+  4 KSP Residual norm 0.0016298
+  5 KSP Residual norm < 1.e-11
Index: petsc/src/sys/dll/reg.c
===================================================================
--- petsc.orig/src/sys/dll/reg.c	2024-12-08 19:16:25.799514284 +0100
+++ petsc/src/sys/dll/reg.c	2024-12-08 19:16:25.795514257 +0100
@@ -20,12 +20,18 @@
   PetscFunctionBegin;
   PetscCall(PetscStrncpy(libs, "${PETSC_LIB_DIR}/libpetsc", sizeof(libs)));
   PetscCall(PetscStrlcat(libs, name, sizeof(libs)));
+  #if defined(PETSC_LIB_NAME_SUFFIX)
+  PetscCall(PetscStrlcat(libs, PETSC_LIB_NAME_SUFFIX, sizeof(libs)));
+  #endif
   PetscCall(PetscDLLibraryRetrieve(PETSC_COMM_WORLD, libs, dlib, 1024, found));
   if (*found) {
     PetscCall(PetscDLLibraryAppend(PETSC_COMM_WORLD, &PetscDLLibrariesLoaded, dlib));
   } else {
     PetscCall(PetscStrncpy(libs, "${PETSC_DIR}/${PETSC_ARCH}/lib/libpetsc", sizeof(libs)));
     PetscCall(PetscStrlcat(libs, name, sizeof(libs)));
+  #if defined(PETSC_LIB_NAME_SUFFIX)
+    PetscCall(PetscStrlcat(libs, PETSC_LIB_NAME_SUFFIX, sizeof(libs)));
+  #endif
     PetscCall(PetscDLLibraryRetrieve(PETSC_COMM_WORLD, libs, dlib, 1024, found));
     if (*found) PetscCall(PetscDLLibraryAppend(PETSC_COMM_WORLD, &PetscDLLibrariesLoaded, dlib));
   }
